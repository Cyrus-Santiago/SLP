#include <stdio.h>
#include <stdlib.h>

//Setup creates an executable file usable in any directory, and allocates a User ID to the user. 
int main(){
	printf("Setting up NueMes Messaging System... \n");
	system("mkdir ~/NueMes");
	system("mkdir ~/NueMes/messages");
	system("cd ~/NueMes");
	system("git init");
	printf("Complete! Use command 'msg' to open System. Happy Messaging!\n");
	return 0;
}
