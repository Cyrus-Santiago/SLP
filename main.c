#include <stdio.h>
#include <string.h>
#include "main.h"

int main(int argc, char *argv[]){
	FILE *help; //Makes certain outputs easier
	char input[20], letter[20], contact[20], s; //Captures Letter Input and possible extra chars
	int e=0, x=0, newFlag=0, viewFlag=0, updateFlag=0, deleteFlag=0, buffer=0; //Sets e to 0 for loop, Flags for functions
	//Command Line Input
	if(argc>=2){
		sscanf(argv[1],"%s", letter); //scans if there is a command line input
		int len=strlen(letter); //Helps sift input
		//Command Line Input Contact
		if(argc==3){
			sscanf(argv[2],"%s", contact); //Scans for contact if user input one
		}
		else{
			contact[0]='0'; //sets to 0 if user did not input contact yet
		}
		//Multiple Letter Input
		if(len==2){
			buffer=1;
			if(letter[0]=='n'){
				newFlag++; //Initialize New Process
			}
			else if(letter[0]=='v'){
				viewFlag++; //Initialize View Process
			}
			else if(letter[0]=='u'){
				updateFlag++; //Initialize Update Process
			}
			else if (letter[0]=='d'){
				deleteFlag++; //Initialize Delete Process
			}
			//Function Calls
			if(letter[1]=='m'){
				x=msg(newFlag, viewFlag, updateFlag, deleteFlag, buffer, contact); //Open messages screen with proper variable values
				if(x==0){
					printf("Goodbye!\n");
					return 0;
				}
			}
			else if(letter[1]=='c'){
				x=ctcs(newFlag, viewFlag, updateFlag, deleteFlag, buffer, contact); //Open contacts screen with proper variable values
				if(x==0){
					printf("Goodbye!\n");
					return 0;
				}
			}
		}
		//Reads Command Line Input
		if(*letter=='m'){
			msg(newFlag, viewFlag, updateFlag, deleteFlag, buffer, contact); //open messages screen
			return 0;
		}
		else if(*letter=='c'){
			ctcs(newFlag, viewFlag, updateFlag, deleteFlag, buffer, contact); //open contacts screen
			return 0;
		}
		else if(*letter=='h'){
			help=fopen("help.txt","r"); //Opens file to print help options
			while((s=fgetc(help))!=EOF){
				printf("%c",s); //Print each character
			}
			fclose(help); //Close file
			printf("> "); //Ready for next input
		}
		else{
			printf("Incorrect Command Line Input! Hint: (use 'msg h' for help)\nSoftware Closing...\n");
			return 0;
		}
	}
	//General Startup
	else{
		printf("*****************************\n");
		printf("Welcome to NueMes Version 1: \n(Use 'h' for list of options) \n"); //Introductory Message (Add Version later)
		printf("*****************************\n");
		printf("> "); //Prompt Input
	}
	do{
		fgets(input,20,stdin); //Takes input, stores in 'input'
		int test = sscanf(input, "%s %s", letter, contact); //Captures letter, number and extra chars (if any)
		int len = strlen(letter); //Helps sift input
		//Multiple Letter Input
		if(len==2){
			buffer=1;
			if(letter[0]=='n'){
				newFlag++; //Initialize New Process
			}
			else if(letter[0]=='v'){
				viewFlag++; //Initialize View Process
			}
			else if(letter[0]=='u'){
				updateFlag++; //Initialize Update Process
			}
			else if (letter[0]=='d'){
				deleteFlag++; //Initialize Delete Process
			}
			//Function Calls
			if(letter[1]=='m'){
				msg(newFlag, viewFlag, updateFlag, deleteFlag, buffer, contact); //Open messages screen with proper variable values
				buffer=0;
			}
			else if(letter[1]=='c'){
				ctcs(newFlag, viewFlag, updateFlag, deleteFlag, buffer, contact); //Open contacts screen with proper variable values
				buffer=0;
			}
		}
		if(*letter=='m'){
			msg(newFlag, viewFlag, updateFlag, deleteFlag, buffer, contact); //open messages screen	
			printf("*************************\n");
			printf(" [Now in Home Menu] \n  (Use 'h' for help) \n");
			printf("*************************\n");
			printf("> ");
		}
		else if(*letter=='c'){
			ctcs(newFlag, viewFlag, updateFlag, deleteFlag, buffer, contact); //open contacts screen
		}
		else if(*letter=='h'){
			help=fopen("help.txt","r"); //Opens file to print help options
			while((s=fgetc(help))!=EOF){
				printf("%c",s); //Print each character
			}
			fclose(help); //Close file
			printf("\n> "); //Ready for next input
		}
		else if(*letter=='x'){
			e++; //Increment out of loop
		}
		else{	
			printf("Invalid option. (Use 'h' for list) \n> ");
		}
	}while(e==0); //Keep loop throughout run
	printf("Goodbye!\n"); //Goodbye Message
	return 0; 
}
