#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "main.h"

void new(char *ctc){
	char message[140], type[5], name[15], directory[30], *file, *path; //140 characters for message
	int i;
	strcpy(type,".txt"); //.txt file
	strcpy(name,ctc); //name of contact
	strcpy(directory,"/home/csantiago/NueMes/messages/"); //~NueMes/messages directory
	file=strcat(name,type); //[contact].txt
	path=strcat(directory,file); //~NueMes/messages/[contact].txt
	FILE *fptr=fopen(path,"a+");
	printf("New Message: \n");
	fgets(message,140,stdin); //Grabs 140 Character Message
	fputs("csantiago: \n",fptr);
	for(i=0;message[i]!='\n';i++){
		fputc(message[i],fptr); //Prints Message in File
	}
	fputc('\n',fptr); //Cleans up file
	fclose(fptr); //closes file after use
	printf("Message Sent! \n");
	return;
}
