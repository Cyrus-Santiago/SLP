all: msg msg_setup respond

msg: main.o msg.o ctcs.o new.o
	gcc -o msg main.o msg.o ctcs.o new.o -g
main.o: main.c main.h
	gcc -c main.c -g
msg.o: msg.c main.h
	gcc -c msg.c -g
ctcs.o: ctcs.c
	gcc -c ctcs.c -g
new.o: new.c
	gcc -c new.c -g
msg_setup: setup.o
	gcc -o msg_setup setup.o -g
setup.o: setup.c
	gcc -c setup.c -g
respond: respond.o
	gcc -o respond respond.o -g
respond.o: respond.c
	gcc -c respond.c -g
clean:
	rm main.o msg.o ctcs.o new.o setup.o respond.o
